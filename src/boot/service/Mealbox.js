import { RequestPost, RequestGet } from "./Template";

const SubmitMealbox = async (mealboxData, customerId, status) => {
  const url = "scan/save";
  let data = new FormData();
  let id_personel = localStorage.getItem("id");
  console.log(mealboxData);
  data.append("id_personel", id_personel);
  data.append("id_pelanggan", customerId);
  data.append("status", status);
  data.append("id_mealbox", mealboxData);

  let res = await RequestPost(url, data);

  return res;
};

const CheckMealboxId = async mealboxId => {
  const url = "scan/check_mealbox_id";
  let res = await RequestGet(url, { id: mealboxId });
  return res;
};

const GetAllCustomer = async () => {
  const url = "scan";
  let res = await RequestGet(url);

  return res;
};
export { SubmitMealbox, CheckMealboxId, GetAllCustomer };
