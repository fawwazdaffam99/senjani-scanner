import Vue from "vue";
import axios from "axios";

Vue.prototype.$axios = axios;
const BaseUrl = "https://dev.senjani.id/service/scanner/api/";
// const BaseUrl = "http://localhost:8080/senjani-scanner-service/api/";

let errorData = {
  status: false,
  message: "Uknown error",
  data: []
};

const RequestPost = async (url, data) => {
  return await axios({
    url: BaseUrl + url,
    method: "POST",
    data: data,
    headers: {
      Accept: "application/json",
      "Content-Type": "multipart/form-data"
    }
  })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      console.log(error);
      return errorData;
    });
};

const RequestDelete = async (url, data) => {
  return await axios({
    url: BaseUrl + url,
    method: "DELETE",
    data: data,
    headers: {
      Accept: "application/json",
      "Content-Type": "multipart/form-data"
    }
  })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      console.log(error);
      return errorData;
    });
};

const RequestGet = async (url, params = {}) => {
  return await axios({
    url: BaseUrl + url,
    method: "GET",
    params,
    headers: {
      Accept: "application/json",
      "Content-Type": "multipart/form-data"
    }
  })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      console.log(error);
      return errorData;
    });
};

export { RequestPost, RequestGet, RequestDelete };
