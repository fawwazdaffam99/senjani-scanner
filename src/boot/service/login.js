import { RequestPost, RequestGet } from "./Template";

const Login = async (email, password) => {
  const url = "login";
  let data = new FormData();
  data.append("email", email);
  data.append("password", password);
  let res = await RequestPost(url, data);
  return res;
};

export { Login };
