const routes = [
  {
    path: "/",
    component: () => import("pages/SplashScreen.vue")
  },
  { path: "/home", component: () => import("pages/Home.vue") },
  {
    path: "/menu",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "/mealbox", component: () => import("pages/Mealbox.vue") },
      {
        path: "/mealboxsuccess",
        component: () => import("pages/MealboxSuccess.vue")
      },
      { path: "/customer", component: () => import("pages/Customer.vue") },
      { path: "/box", component: () => import("pages/Box.vue") },
      {
        path: "/circulation",
        component: () => import("pages/Circulation.vue")
      },
      { path: "/team", component: () => import("pages/Team.vue") }
    ]
  },
  {
    path: "/login",
    component: () => import("pages/Login.vue")
  },
  {
    path: "/scan",
    component: () => import("pages/Scan.vue")
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Home.vue")
  });
}

export default routes;
