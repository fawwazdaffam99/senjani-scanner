import Vue from "vue";
import Vuex from "vuex";
import mealbox from "./mealbox";
import admin from "./admin";
// import example from './module-example'

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function(/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      mealbox,
      admin
    }

    // enable strict mode (adds overhead!)
    // for dev mode only
    // strict: process.env.DEV
  });
  // if (process.env.DEV && module.hot) {
  //   module.hot.accept(['./mealbox'], () => {
  //     const newShowcase = require('./mealbox').default
  //     Store.hotUpdate({ modules: { mealbox: newMealbox } })
  //   })
  // }
  return Store;
}
