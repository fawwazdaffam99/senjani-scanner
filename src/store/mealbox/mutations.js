export const updateMealboxState = (state, params) => {
    state.mealboxState = params
}
export const updateMealboxData = (state, params) => {
    state.mealboxData = params
}
export const updateMealboxCustomerData = (state, params) => {
    state.mealboxCustomerData = params
}
export const updateMealboxCustomerList = (state, params) => {
    state.mealboxCustomerList = params
}
export const updateMealboxCustomerId = (state, params) => {
    state.mealboxCustomerId = params
}
export const updateMealboxCustomerName = (state, params) => {
    state.mealboxCustomerName = params
}

