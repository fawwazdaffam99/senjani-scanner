/*
export function someMutation (state) {
}
*/

//role
export function GET_ROLE(state) {
  state.role.status = {
    loading: true,
    value: false
  };
}
export function GET_ROLE_SUCCESS(state, data) {
  state.role.data = data;
  state.role.status = {
    loading: false,
    valrole
  };
}
export function GET_ROLE_FAILED(state, data) {
  state.role.data = data;
  state.role.status = {
    loading: false,
    value: false
  };
}

//customer
export function GET_CUSTOMER(state) {
  state.customer.status = {
    loading: true,
    value: false
  };
}
export function GET_CUSTOMER_SUCCESS(state, data) {
  state.customer.data = data;
  state.customer.status = {
    loading: false,
    value: true
  };
}
export function GET_CUSTOMER_FAILED(state, data) {
  state.customer.data = data;
  state.customer.status = {
    loading: false,
    value: false
  };
}

export function ADD_CUSTOMER(state) {
  state.customer.add.status = {
    loading: true,
    value: false
  };
}
export function ADD_CUSTOMER_SUCCESS(state, data) {
  state.customer.add.data = data;
  state.customer.add.status = {
    loading: false,
    value: true
  };
}
export function ADD_CUSTOMER_FAILED(state, data) {
  state.customer.add.data = data;
  state.customer.add.status = {
    loading: false,
    value: false
  };
}
export function DELETE_CUSTOMER(state) {
  state.customer.delete.status = {
    loading: true,
    value: false
  };
}
export function DELETE_CUSTOMER_SUCCESS(state, data) {
  state.customer.delete.data = data;
  state.customer.delete.status = {
    loading: false,
    value: true
  };
}
export function DELETE_CUSTOMER_FAILED(state, data) {
  state.customer.delete.data = data;
  state.customer.delete.status = {
    loading: false,
    value: false
  };
}

//team
export function GET_TEAM(state) {
  state.team.status = {
    loading: true,
    value: false
  };
}
export function GET_TEAM_SUCCESS(state, data) {
  state.team.data = data;
  state.team.status = {
    loading: false,
    value: true
  };
}
export function GET_TEAM_FAILED(state, data) {
  state.team.data = data;
  state.team.status = {
    loading: false,
    value: false
  };
}

export function ADD_TEAM(state) {
  state.team.add.status = {
    loading: true,
    value: false
  };
}
export function ADD_TEAM_SUCCESS(state, data) {
  state.team.add.data = data;
  state.team.add.status = {
    loading: false,
    value: true
  };
}
export function ADD_TEAM_FAILED(state, data) {
  state.team.add.data = data;
  state.team.add.status = {
    loading: false,
    value: false
  };
}
export function DELETE_TEAM(state) {
  state.team.delete.status = {
    loading: true,
    value: false
  };
}
export function DELETE_TEAM_SUCCESS(state, data) {
  state.team.delete.data = data;
  state.team.delete.status = {
    loading: false,
    value: true
  };
}
export function DELETE_TEAM_FAILED(state, data) {
  state.team.delete.data = data;
  state.team.delete.status = {
    loading: false,
    value: false
  };
}

//circulation
export function GET_CIRCULATION(state) {
  state.circulation.status = {
    loading: true,
    value: false
  };
}
export function GET_CIRCULATION_SUCCESS(state, data) {
  state.circulation.data = data;
  state.circulation.status = {
    loading: false,
    value: true
  };
}
export function GET_CIRCULATION_FAILED(state, data) {
  state.circulation.data = data;
  state.circulation.status = {
    loading: false,
    value: false
  };
}
export function GET_DETAIL_CIRCULATION_BY_BOX(state) {
  state.circulation.detailByBox.status = {
    loading: true,
    value: false
  };
}
export function GET_DETAIL_CIRCULATION_BY_BOX_SUCCESS(state, data) {
  state.circulation.detailByBox.data = data;
  state.circulation.detailByBox.status = {
    loading: false,
    value: false
  };
}
export function GET_DETAIL_CIRCULATION_BY_BOX_FAILED(state, data) {
  state.circulation.detailByBox.data = data;
  state.circulation.detailByBox.status = {
    loading: false,
    value: false
  };
}

//box
export function GET_BOX(state) {
  state.box.status = {
    loading: true,
    value: false
  };
}
export function GET_BOX_SUCCESS(state, data) {
  state.box.data = data;
  state.box.status = {
    loading: false,
    value: true
  };
}
export function GET_BOX_FAILED(state, data) {
  state.box.data = data;
  state.box.status = {
    loading: false,
    value: false
  };
}

export function ADD_BOX(state) {
  state.box.add.status = {
    loading: true,
    value: false
  };
}
export function ADD_BOX_SUCCESS(state, data) {
  state.box.add.data = data;
  state.box.add.status = {
    loading: false,
    value: true
  };
}
export function ADD_BOX_FAILED(state, data) {
  state.box.add.data = data;
  state.box.add.status = {
    loading: false,
    value: false
  };
}

export function DELETE_BOX(state) {
  state.box.delete.status = {
    loading: true,
    value: false
  };
}
export function DELETE_BOX_SUCCESS(state, data) {
  state.box.delete.data = data;
  state.box.delete.status = {
    loading: false,
    value: true
  };
}
export function DELETE_BOX_FAILED(state, data) {
  state.box.delete.data = data;
  state.box.delete.status = {
    loading: false,
    value: false
  };
}
