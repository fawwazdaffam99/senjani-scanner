export default function() {
  const initialData = {
    data: [],
    status: {
      loading: false,
      value: false
    }
  };

  return {
    customer: {
      data: [],
      status: {
        loading: false,
        value: false
      },
      add: initialData,
      delete: initialData
    },
    role: {
      data: [],
      status: {
        loading: false,
        value: false
      }
    },
    team: {
      data: [],
      status: {
        loading: false,
        value: false
      },
      add: initialData,
      delete: initialData
    },
    box: {
      data: [],
      status: {
        loading: false,
        value: false
      },
      add: initialData,
      delete: initialData
    },
    circulation: {
      data: [],
      status: {
        loading: false,
        value: false
      },
      add: initialData,
      detailByBox: initialData
    }
  };
}
