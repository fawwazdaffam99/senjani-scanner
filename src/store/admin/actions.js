import {
  RequestGet,
  RequestPost,
  RequestDelete
} from "../../boot/service/Template";

//role
export async function getRoleData(ctx) {
  ctx.commit("GET_ROLE");
  const res = await RequestGet("admin/jabatan");
  if (!res.status) {
    ctx.commit("GET_ROLE_SUCCESS", res);
  } else {
    ctx.commit("GET_ROLE_FAILED", res);
  }
}

export async function addRoleData(ctx, payload = {}) {
  ctx.commit("ADD_ROLE");
  const formData = new FormData();
  formData.append("nama_lengkap", payload.nama);
  formData.append("no_hp", payload.no_hp);
  const res = await RequestPost("admin/jabatan", formData);
  if (!res.status) {
    ctx.commit("ADD_ROLE_SUCCESS", res);
  } else {
    ctx.commit("ADD_ROLE_FAILED", res);
  }
}

//customer
export async function getCustomerData(ctx) {
  ctx.commit("GET_CUSTOMER");
  const res = await RequestGet("admin/pelanggan");
  if (!res.status) {
    ctx.commit("GET_CUSTOMER_SUCCESS", res);
  } else {
    ctx.commit("GET_CUSTOMER_FAILED", res);
  }
}

export async function addCustomerData(ctx, payload = {}) {
  ctx.commit("ADD_CUSTOMER");
  const formData = new FormData();
  formData.append("nama_lengkap", payload.nama);
  formData.append("no_hp", payload.no_hp);
  const res = await RequestPost("admin/pelanggan", formData);
  if (!res.status) {
    ctx.commit("ADD_CUSTOMER_SUCCESS", res);
  } else {
    ctx.commit("ADD_CUSTOMER_FAILED", res);
  }
}

export async function deleteCustomerData(ctx, payload = {}) {
  ctx.commit("DELETE_CUSTOMER");
  const formData = new FormData();
  formData.append("id", payload.id);
  const res = await RequestPost("admin/pelanggan/delete_pelanggan", formData);
  if (!res.status) {
    ctx.commit("DELETE_CUSTOMER_SUCCESS", res);
  } else {
    ctx.commit("DELETE_CUSTOMER_FAILED", res);
  }
}
//team
export async function getTeamData(ctx) {
  ctx.commit("GET_TEAM");
  const res = await RequestGet("admin/personel");
  if (!res.status) {
    ctx.commit("GET_TEAM_SUCCESS", res);
  } else {
    ctx.commit("GET_TEAM_FAILED", res);
  }
}

export async function addTeamData(ctx, payload = {}) {
  ctx.commit("ADD_TEAM");
  const formData = new FormData();
  formData.append("nama_lengkap", payload.nama_lengkap);
  formData.append("id_jabatan", payload.id_jabatan);
  formData.append("email", payload.email);
  formData.append("password", payload.password);
  const res = await RequestPost("admin/personel", formData);
  if (!res.status) {
    ctx.commit("ADD_TEAM_SUCCESS", res);
  } else {
    ctx.commit("ADD_TEAM_FAILED", res);
  }
}

export async function deleteTeamData(ctx, payload = {}) {
  ctx.commit("DELETE_TEAM");
  const formData = new FormData();
  formData.append("id", payload.id);

  const res = await RequestPost("admin/personel/delete_personel", formData);
  if (!res.status) {
    ctx.commit("DELETE_TEAM_SUCCESS", res);
  } else {
    ctx.commit("DELETE_TEAM_FAILED", res);
  }
}

//box
export async function getBoxData(ctx) {
  ctx.commit("GET_BOX");
  const res = await RequestGet("admin/mealbox");
  if (!res.status) {
    ctx.commit("GET_BOX_SUCCESS", res);
  } else {
    ctx.commit("GET_BOX_FAILED", res);
  }
}

export async function addBoxData(ctx, payload = {}) {
  ctx.commit("ADD_BOX");
  const formData = new FormData();
  formData.append("jenis", payload.jenis);
  formData.append("keterangan", payload.keterangan);
  const res = await RequestPost("admin/mealbox", formData);
  if (!res.status) {
    ctx.commit("ADD_BOX_SUCCESS", res);
  } else {
    ctx.commit("ADD_BOX_FAILED", res);
  }
}

export async function deleteBoxData(ctx, payload = {}) {
  ctx.commit("DELETE_BOX");
  const formData = new FormData();
  formData.append("id", payload.id);
  const res = await RequestPost("admin/mealbox/delete_mealbox", formData);
  if (!res.status) {
    ctx.commit("DELETE_BOX_SUCCESS", res);
  } else {
    ctx.commit("DELETE_BOX_FAILED", res);
  }
}

//circulation
export async function getCirculationData(ctx) {
  ctx.commit("GET_CIRCULATION");
  const res = await RequestGet("admin/sirkulasi");
  if (!res.status) {
    ctx.commit("GET_CIRCULATION_SUCCESS", res);
  } else {
    ctx.commit("GET_CIRCULATION_FAILED", res);
  }
}
export async function getDetailCirculationByBoxData(ctx, payload = {}) {
  ctx.commit("GET_DETAIL_CIRCULATION_BY_BOX");
  const formData = new FormData();
  formData.append("id_pelanggan", payload.id_pelanggan);
  formData.append("jumlah_mealbox", payload.jumlah_mealbox);
  const res = await RequestPost("admin/pelanggan/mealbox_pelanggan", formData);
  if (!res.status) {
    ctx.commit("GET_DETAIL_CIRCULATION_BY_BOX_SUCCESS", res);
  } else {
    ctx.commit("GET_DETAIL_CIRCULATION_BY_BOX_FAILED", res);
  }
}
